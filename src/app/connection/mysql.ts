import knex from 'knex';
import config from '../config';

export const connection = knex({
  client: 'mysql2',
  connection: {
    host: config.db.HOST,
    database: config.db.DATABASE,
    port: +config.db.PORT,
    user: config.db.USER,
    password: config.db.PASSWORD,
  },
});

export const connection2 = knex({
  client: 'mysql2',
  connection: {
    host: config.db2.HOST,
    database: config.db2.DATABASE,
    port: +config.db2.PORT,
    user: config.db2.USER,
    password: config.db2.PASSWORD,
  },
});

// export default connection;
