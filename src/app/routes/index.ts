/* eslint-disable prettier/prettier */
import { Router } from 'express';

// Import Routes
import CompanyBank from '../services/company_bank/_routes.company_bank';
import Company from '../services/company/_routes.company';
import Branch from '../services/branch/_routes.branch';

const router = Router();

export default {
  // product: router.use('/product', product),
  companyBank: router.use('/administration/company-bank', CompanyBank),
  branch: router.use('/administration/branch', Branch),
  company: router.use('/administration/company', Company),
  // administration: router.use('/administration', product),
};
