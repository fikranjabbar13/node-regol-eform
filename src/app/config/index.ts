/* eslint-disable prettier/prettier */
// eslint-disable-next-line prettier/prettier
export default {
  app: {
    HOST: process.env.APP_HOST || '0.0.0.0',
    PORT: process.env.APP_PORT || 4444,
  },
  db: {
    HOST: process.env.DB_HOST || 'localhost',
    // HOST: process.env.DB_HOST || 'ndb02stg.leekie.com',
    DATABASE: process.env.DB_NAME || 'phoenixLokal',
    PORT: process.env.DB_PORT || 3306,
    USER: process.env.DB_USER || 'root',
    PASSWORD: process.env.DB_PASS || 'asdf1234*',
  },
  db2: {
    HOST: process.env.DB_HOST || 'localhost',
    // HOST: process.env.DB_HOST || 'ndb02stg.leekie.com',
    DATABASE: process.env.DB_NAME || 'phoenixLokal',
    PORT: process.env.DB_PORT || 3306,
    USER: process.env.DB_USER || 'root',
    PASSWORD: process.env.DB_PASS || '',
  },
};
