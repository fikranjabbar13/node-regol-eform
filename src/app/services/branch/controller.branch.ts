import { Request, Response, NextFunction } from 'express';
import { ProductServices } from './services.branch';
import ProductValidator from './validator.branch';

const services = new ProductServices();
const validator = new ProductValidator();

export class BranchController {
  async getListBranch(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await services.getListBranch({
        id: 'GdLMj',
        page: (req.query.paginationPage as unknown) as number,
        row: (req.query.paginationRows as unknown) as number,
      });
      const newResult = {
        rows: result,
        total: result.length,
      };
      res.json(newResult);
    } catch (error) {
      next(error);
    }
  }

  async insertProduct(req: Request, res: Response, next: NextFunction) {
    try {
      const validate = await validator.insertValidate({
        branch: req.body.branch,
        city: req.body.city,
        address: req.body.address,
        phone: req.body.phone,
        fax: req.body.fax,
        head: req.body.head,
        headQuarter: req.body.head_quarter,
        active: req.body.active,
      });
      // const result = await services.insertProduct({
      //   branch: req.body.branch,
      //   city: req.body.city,
      //   address: req.body.address,
      //   phone: req.body.phone,
      //   fax: req.body.fax,
      //   head: req.body.head,
      //   headQuarter: req.body.head_quarter,
      //   active: req.body.active,
      // });
      res.json(validate);
    } catch (error) {
      next(error);
    }
  }

  async deleteProduct(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await services.deleteProduct(+req.params.id);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }

  async updateProductById(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await services.updateProductById({
        companyID: 'GdLMj',
        branchID: 'OqloR',
        conflictCourtID: 0,
        branchName: 'Balikpapan',
        branchHeadName: "Nueva.,,,.,'''asda test",
        isHeadQuarter: 1,
        isActive: 1,
        stepFiveCity: 'aaa',
        branchOrder: 1,
        city: 'Makati',
        address: 'Yes\nNo',
        phoneAreaCode: '123',
        phone: '123',
        faxAreaCode: '11',
        fax: '87000',
        minDemoAccount: 10,
        isDemoAuthentication: 0,
        minDemoAccountPBK: 50,
        isDemoAuthenticationPBK: 0,
        branchAgreementCode: 'DB',
        isCustomerTaxNumberRequired: 1,
        isCustomerTaxNumberFixed: 1,
        postalCode: '12312321',
        emailAddress: 'nueva.phoenix@leekie.com',
        traderEmailAddress: 'nueva.phoenix@leekie.com',
      });
      res.json(result);
    } catch (error) {
      next(error);
    }
  }
}
