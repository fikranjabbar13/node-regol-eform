import { connection } from '../../connection/mysql';

export interface Branch {
  branch: string;
  city: string;
  address: string;
  phone: string;
  fax: string;
  head: string;
  headQuarter: string;
  active: string;
}

export interface BranchUpdate {
  companyID: string;
  branchID: string;
  conflictCourtID: number;
  branchName: string;
  branchHeadName: string;
  isHeadQuarter: number;
  isActive: number;
  stepFiveCity: string;
  branchOrder: number;
  city: string;
  address: string;
  phoneAreaCode: string;
  phone: string;
  faxAreaCode: string;
  fax: string;
  minDemoAccount: number;
  isDemoAuthentication: number;
  minDemoAccountPBK: number;
  isDemoAuthenticationPBK: number;
  branchAgreementCode: string;
  isCustomerTaxNumberRequired: number;
  isCustomerTaxNumberFixed: number;
  postalCode: string;
  emailAddress: string;
  traderEmailAddress: string;
}

export interface Paging {
  id: string;
  page: number;
  row: number;
}

export class ProductServices {
  /**
   * Get all branch data
   * with pagination
   */
  getListBranch(paging: Paging = { id: 'GdLMj', row: 20, page: 1 }) {
    return connection('branch')
      .limit(paging.row)
      .offset((paging.page - 1) * paging.row);
  }

  /**
   * Insert Branch
   * @param data
   */
  async insertProduct(data: Branch) {
    // return this.data.push(data);
    const result = await connection('branch').insert({
      branch: data.branch,
      city: data.city,
      address: data.address,
      phone: data.phone,
      fax: data.fax,
      head: data.head,
      head_quarter: data.headQuarter,
      active: data.active,
    });

    return {
      message: 'Success Insert',
    };

    // jika ke procedure
    return connection.raw('CALL create_branch(?, ?, ?, ? ,? ,? ,? ,?)', [
      data.branch,
      data.city,
      data.address,
      data.phone,
      data.fax,
      data.head,
      data.headQuarter,
      data.active,
    ]);
  }

  /**
   * @param id
   */
  deleteProduct(id: number) {
    // return this.data.filter((v: ProductInterfaceFull) => v.id !== id);

    connection('branch').delete().where({
      branchId: id,
    });
  }

  /**
   * Update Product by id
   * @param id
   * @param updatedData
   */
  async updateProductById(updatedData: BranchUpdate) {
    const result = await connection('branch')
      .update({
        branchName: updatedData.branchName,
      })
      .where({
        // branchID: 'd4903db4-a92e-11e3-a4d9-005056810073',
        branchID: updatedData.branchID,
      });

    return result;
  }
}
