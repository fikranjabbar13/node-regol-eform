import { Router } from 'express';
import { BranchController } from './controller.branch';

const router = Router();
const controller = new BranchController();

/**
 * @swagger
 * /api/product:
 *    get:
 *     tags:
 *     - "product"
 *     summary: Retrieve a list of products.
 *     description: Retrieve a list of products.
 *     responses:
 *       200:
 *         description: A list of product.
 */
router.get('/get-list', controller.getListBranch);
router.post('/', controller.insertProduct);
router.delete('/:id', controller.deleteProduct);
router.post('/branch/update', controller.updateProductById);

export default router;
