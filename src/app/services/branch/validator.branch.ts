import Joi from 'joi';
import HttpExpection from '../../errors/HttpExpection';
class ProductValidator {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  insertValidate(data: any) {
    const schema = Joi.object({
      branch: Joi.string().alphanum().min(3).max(30).required(),
      city: Joi.string().alphanum().min(3).max(30).required(),
      address: Joi.string().alphanum().min(3).max(30).required(),
      phone: Joi.string().alphanum().min(3).max(30).required(),
      fax: Joi.string().alphanum().min(3).max(30).required(),
      head: Joi.string().alphanum().min(3).max(30).required(),
      headQuarter: Joi.string().alphanum().min(3).max(30).required(),
      active: Joi.string().alphanum().min(3).max(30).required(),
    });

    const result = schema.validate(data);

    if (result.error) {
      throw new HttpExpection(400, { message: 'Fail validate', data: result });
    } else return result.value;
    // -> { value: { username: 'abc', birth_year: 1994 } }
  }
}

export default ProductValidator;
