import { Router } from 'express';
import { BankController } from './controller.company_bank';

const router = Router();
const controller = new BankController();

/**
 * @swagger
 * /administration/company-bank/get-list:
 *    get:
 *     tags:
 *     - "bank"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
 *         description: A list of company.
 */
router.get('/get-list', controller.getDataBank);
/**
 * @swagger
 * /administration/company-bank/get-item:
 *    get:
 *     tags:
 *     - "bank"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
 *         description: A list of company.
 */
router.get('/get-item', controller.getBankcurrencyDataAndEdit);

/**
 * @swagger
 * /administration/company-bank/create:
 *    get:
 *     tags:
 *     - "company-bank"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
//  *         description: A list of company.
 */
router.post('/create', controller.insertCompanyBank);
/**
 * @swagger
 * /administration/company-bank/update:
 *    get:
 *     tags:
 *     - "company-bank"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
//  *         description: A list of company.
 */
router.put('/update', controller.updateCompanyBank);

// list laravel
// Route::get('/get-list', "CompanyBankController@getList");
//       Route::get(
//         '/get-active-accounts',
//         "CompanyBankController@getActiveAccounts",
//       );
//       Route::get('/get-item', "CompanyBankController@getItem");
//       Route::post('/create', "CompanyBankController@create");
//       Route::post('/update', "CompanyBankController@update");
//       Route::post('/delete', "CompanyBankController@delete");
//       Route::post('/create-bank', "CompanyBankController@createBank");

export default router;
