import { NextFunction, Request, Response } from 'express';
import { CompanybankData, BankServices, CompanybankDataUpdate } from './services.company_bank';

import CompanyBankValidator from './validator.company_bank';
import Hashids from 'hashids/cjs';

const servicesBank = new BankServices();
const validatorCompanyBank = new CompanyBankValidator();
const hashids = new Hashids();
export class BankController {
  async getDataBank(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await servicesBank.getBankData({
        id: 'GdLMj',
        page: (req.query.paginationPage as unknown) as number,
        row: (req.query.paginationRows as unknown) as number,
      });
      const newResult = {
        rows: result,
        total: result.length,
      };
      res.json(newResult);
    } catch (error) {
      next(error);
    }
  }
  async getBankcurrencyDataAndEdit(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await servicesBank.getBankDataEdit(req.query.id as string);
      const banks = await servicesBank.getBank();
      const currency = await servicesBank.getCurrency();
      const newResult = {
        bank: banks,
        currency: currency,
      };
      if (!req.query.id) {
        res.json(newResult);
      } else {
        res.json(
          result.map((val) => {
            return {
              ...val,
              bank: banks.map((val) => val),
              currency: currency.map((val) => val),
            };
          }),
        );
      }
    } catch (error) {
      next(error);
    }
  }

  async insertCompanyBank(req: Request, res: Response, next: NextFunction) {
    try {
      const validateResult: CompanybankData = validatorCompanyBank.insertValidate(req.body);
      const result = await servicesBank.insertCompanyBankData(validateResult);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }

  async updateCompanyBank(req: Request, res: Response, next: NextFunction) {
    try {
      const validateResult: CompanybankDataUpdate = validatorCompanyBank.updateValidate(req.body);
      const result = await servicesBank.updateCompanyData(validateResult);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }
}
