import Joi from 'joi';
import HttpExpection from '../../errors/HttpExpection';
class CompanyBankValidator {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  insertValidate(data: any) {
    const schema = Joi.object({
      accountNumber: Joi.string().min(3).max(30).required(),
      bankID: Joi.number().required(),
      companyID: Joi.number().required(),
      currencyCode: Joi.string().min(3).max(30).required(),
      isActive: Joi.number().required(),
      timestampCreated: Joi.string().min(3).max(30).required(),
    });

    const result = schema.validate(data);

    if (result.error) {
      throw new HttpExpection(400, { message: 'Fail validate', data: result });
    } else return result.value;
    // -> { value: { username: 'abc', birth_year: 1994 } }
  }
  updateValidate(data: any) {
    const schema = Joi.object({
      accountNumber: Joi.string().min(3).max(30).required(),
      bankID: Joi.number().required(),
      companyID: Joi.number().required(),
      currencyCode: Joi.string().min(3).max(30).required(),
      isActive: Joi.number().required(),
      companyBankID: Joi.number().required(),
    });

    const result = schema.validate(data);

    if (result.error) {
      throw new HttpExpection(400, { message: 'Fail validate', data: result });
    } else return result.value;
    // -> { value: { username: 'abc', birth_year: 1994 } }
  }
}

export default CompanyBankValidator;
