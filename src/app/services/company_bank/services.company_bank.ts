import { connection } from '../../connection/mysql';
import Hashids from 'hashids/cjs';

export interface Paging {
  id: string;
  page: number;
  row: number;
}
export interface CompanybankData {
  accountNumber: string;
  bankID: number;
  companyID: number;
  currencyCode: string;
  isActive: number;
  timestampCreated: string;
}
export interface CompanybankDataUpdate {
  accountNumber: string;
  bankID: number;
  companyBankID: number;
  companyID: number;
  currencyCode: string;
  isActive: number;
}

export interface CompanybankDatadelete {
  companyBankID: number;
  timestampDeleted?: any;
}

const hashids = new Hashids();
export class BankServices {
  /**
   * Get company bank list
   *
   * @return JsonResponse
   */
  async getBankData(paging: Paging = { id: 'GdLMj', row: 20, page: 1 }) {
    return connection('company_bank_account as cba')
      .select('accountNumber', 'bankBranch', 'bankName', 'companyBankID', 'currencyCode', 'isActive')
      .limit(paging.row)
      .offset((paging.page - 1) * paging.row)
      .join('bank as ba', 'ba.bankID', '=', 'cba.bankID')
      .where('cba.companyID', 1)
      .whereNull('cba.timestampDeleted');
  }
  /**
   * Get company bank list
   *
   * @return JsonResponse
   */
  async getBankDataEdit(id?: string) {
    const company_bank = connection('company_bank_account as cba')
      .join('bank as ba', 'ba.bankID', '=', 'cba.bankID')
      .where({ accountNumber: id })
      .select(
        'cba.companyBankID',
        'cba.bankID',
        'ba.bankName',
        'ba.bankBranch',
        'ba.accountNumberFormat',
        'ba.accountNumberSample',
        'cba.currencyCode',
        'cba.accountNumber',
        'cba.isActive',
      );
    return id ? company_bank : this.getBank();
  }
  async getBank() {
    const BANKCONNECTION = connection('bank').select(
      'bankBranch as branch',
      'accountNumberFormat as format',
      'bankName as label',
      'accountNumberSample as sample',
      'bankID as value',
    );
    return BANKCONNECTION;
  }

  async getCurrency() {
    const getCurrency = [
      {
        label: 'USD',
        value: 'USD',
      },
      {
        label: 'IDR',
        value: 'IDR',
      },
    ];
    return getCurrency;
  }
  async insertCompanyBankData(companyData: CompanybankData) {
    try {
      const { accountNumber, bankID, companyID, currencyCode, isActive, timestampCreated } = companyData;
      await connection('company_bank_account').insert({
        accountNumber,
        bankID,
        companyID,
        currencyCode,
        isActive,
        timestampCreated,
      });
      return 'New Company Bank has been successfully added!';
    } catch (error) {
      console.log(error);
      // return 'ERROR: ' + error.sqlMessage;
    }
  }

  async updateCompanyData(companyData: CompanybankDataUpdate) {
    try {
      const { accountNumber, bankID, companyBankID, companyID, currencyCode, isActive } = companyData;
      await connection('company_bank_account')
        .update({
          accountNumber,
          bankID,
          companyBankID,
          companyID,
          currencyCode,
          isActive,
        })
        .where({
          companyBankID,
        });
      return 'Company Bank record has been successfully updated!';
    } catch (error) {
      console.log(error);
      // return 'ERROR: ' + error.sqlMessage;
    }
  }
  //ini belum
  async deleteCompanybankById(updatedData: CompanybankDatadelete) {
    const { companyBankID, timestampDeleted } = updatedData;

    await connection('company_bank_account')
      // .update({
      //   timestampDeleted: updatedData.timestampDeleted,
      // })
      .where({
        // branchID: 'd4903db4-a92e-11e3-a4d9-005056810073',
        companyBankID: updatedData.companyBankID,
      });

    return 'Company Bank record has been successfully deleted!';
  }
  async getBank89() {
    const id = hashids.encode([1]);
    return id;
    // return connection('company_bank_account');
  }
}
(async function () {
  const a = new BankServices();
  // console.log(connection());

  // console.log(await a.getBankDataEdit('325'));
  // console.log(await a.getBank89());

  // console.log(await (await a.getBankData()).length);

  // insertCompanyBankData
  // console.log(
  //   await a.insertCompanyBankData({
  //     accountNumber: '134-800',
  //     bankID: 8,
  //     companyID: 1,
  //     currencyCode: 'IDR',
  //     isActive: 0,
  //     timestampCreated: '2020-12-18 15:54:56',
  //   }),
  // );

  // deleteCompanyBank
  // console.log(
  //   await a.updateCompanyData({
  //     accountNumber: '123-653',
  //     bankID: 8,
  //     companyBankID: 133,
  //     companyID: 1,
  //     currencyCode: 'IDR',
  //     isActive: 1,
  //   }),
  // );
})();
