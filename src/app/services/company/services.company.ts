import { connection } from '../../connection/mysql';
export interface CompanyData {
  companyID?: string;
  company_unit_id: string;
  company_phone: string;
  company_fax: string;
  company_address: string;
  company_email: string;
  senderEmail: string;
  companyRegistrationEmail: string;
  company_court: string;
  companyCode: string;
  companyName: string;
  companyLink: string;
  etradeDemoLink: string;
  etradeRealLink: string;
  senderName: string;
  smtpHost: string;
  smtpPort: number;
  smtpUser: string;
  smtpPass: string;
  bappebtiLicense: string;
  futuresExchangeMembership: string;
  futuresClearingMembership: string;
  approvalTradingParticipantSystem: string;
  implementationAlternativeTradingSystem: string;
  website: string;
  ceo: string;
  director: string;
  complianceDirector: string;
  eCaptchaPrivateKey: string;
  reCaptchaPublicKey: string;
  timeStampCreated?: string;
  backupDate: string | null;
  phoenixCompanyID: number;
}

export class CompanyService {
  async getCompanyData(id?: string) {
    return id ? connection('company').where({ companyID: id }) : connection('company');
  }

  async insertCompanyData(companyData: CompanyData) {
    try {
      const {
        companyID,
        company_unit_id,
        company_phone,
        company_fax,
        company_address,
        company_email,
        senderEmail,
        companyRegistrationEmail,
        company_court,
        companyCode,
        companyName,
        companyLink,
        etradeDemoLink,
        etradeRealLink,
        senderName,
        smtpHost,
        smtpPort,
        smtpUser,
        smtpPass,
        bappebtiLicense,
        futuresExchangeMembership,
        futuresClearingMembership,
        approvalTradingParticipantSystem,
        implementationAlternativeTradingSystem,
        website,
        ceo,
        director,
        complianceDirector,
        eCaptchaPrivateKey,
        reCaptchaPublicKey,
        timeStampCreated,
        backupDate,
        phoenixCompanyID,
      } = companyData;
      await connection('company').insert({
        companyID,
        company_unit_id,
        company_phone,
        company_fax,
        company_address,
        company_email,
        senderEmail,
        companyRegistrationEmail,
        company_court,
        companyCode,
        companyName,
        companyLink,
        etradeDemoLink,
        etradeRealLink,
        senderName,
        smtpHost,
        smtpPort,
        smtpUser,
        smtpPass,
        bappebtiLicense,
        futuresExchangeMembership,
        futuresClearingMembership,
        approvalTradingParticipantSystem,
        implementationAlternativeTradingSystem,
        website,
        ceo,
        director,
        complianceDirector,
        eCaptchaPrivateKey,
        reCaptchaPublicKey,
        timeStampCreated,
        backupDate,
        phoenixCompanyID,
      });
      return 'sukses insert';
    } catch (error) {
      console.log(error);
      // return 'ERROR: ' + error.sqlMessage;
    }
  }
  // async getCompanyData() {
  //   return connection('company');
  // }
  // async updateCompanyData(companyID: string, companyData: CompanyData) {
  //   try {
  //     const {
  //       company_unit_id,
  //       company_phone,
  //       company_fax,
  //       company_address,
  //       company_email,
  //       senderEmail,
  //       companyRegistrationEmail,
  //       company_court,
  //       companyCode,
  //       companyName,
  //       companyLink,
  //       etradeDemoLink,
  //       etradeRealLink,
  //       senderName,
  //       smtpHost,
  //       smtpPort,
  //       smtpUser,
  //       smtpPass,
  //       bappebtiLicense,
  //       futuresExchangeMembership,
  //       futuresClearingMembership,
  //       approvalTradingParticipantSystem,
  //       implementationAlternativeTradingSystem,
  //       website,
  //       ceo,
  //       director,
  //       complianceDirector,
  //       eCaptchaPrivateKey,
  //       reCaptchaPublicKey,
  //       timeStampCreated,
  //       backupDate,
  //       phoenixCompanyID,
  //     } = companyData;
  //     await connection('company')
  //       .update({
  //         company_unit_id,
  //         company_phone,
  //         company_fax,
  //         company_address,
  //         company_email,
  //         senderEmail,
  //         companyRegistrationEmail,
  //         company_court,
  //         companyCode,
  //         companyName,
  //         companyLink,
  //         etradeDemoLink,
  //         etradeRealLink,
  //         senderName,
  //         smtpHost,
  //         smtpPort,
  //         smtpUser,
  //         smtpPass,
  //         bappebtiLicense,
  //         futuresExchangeMembership,
  //         futuresClearingMembership,
  //         approvalTradingParticipantSystem,
  //         implementationAlternativeTradingSystem,
  //         website,
  //         ceo,
  //         director,
  //         complianceDirector,
  //         eCaptchaPrivateKey,
  //         reCaptchaPublicKey,
  //         timeStampCreated,
  //         backupDate,
  //         phoenixCompanyID,
  //       })
  //       .where({
  //         companyID,
  //       });
  //     return 'sukses Update';
  //   } catch (error) {
  //     console.log(error);
  //     // return 'ERROR: ' + error.sqlMessage;
  //   }
  // }
}

(async function () {
  const a = new CompanyService();
  // console.log(await a.getCompanyDataById('67aafbe6-d082-11e3-953d-00505681fuji'));
  // console.log(
  //   await a.updateCompanyData('67aafbe6-d082-11e3-953d-00505681fuji', {
  //     company_unit_id: 'bf2414f8-a84a-11e3-a4d9-005056810073',
  //     company_phone: '021-29675088',
  //     company_fax: '021-29675089',
  //     company_address: 'TCC Batavia Tower One Lt. 10\r\nJl. KH. Mansyur Kav. 126 Jakarta Pusat',
  //     company_email: 'berjangka@solidgold.co.id',
  //     senderEmail: 'noreply@solidgoldberjangka.co.id',
  //     companyRegistrationEmail: 'registrasi@solidgold.co.id',
  //     company_court: 'Pengadilan Negeri Jakarta Pusat',
  //     companyCode: 'SGB',
  //     companyName: 'PT. Andry Sandy',
  //     companyLink: 'https://regol.solidgold.co.id/signup',
  //     etradeDemoLink: '',
  //     etradeRealLink: '',
  //     senderName: 'Registrasi Online PT SGB',
  //     smtpHost: 'mail.solidgoldberjangka.co.id',
  //     smtpPort: 25,
  //     smtpUser: 'noreply@solidgoldberjangka.co.id',
  //     smtpPass: 'noreplysgb123',
  //     bappebtiLicense: '161/BAPPEBTI/SI/IX/2002',
  //     futuresExchangeMembership: 'SPAB-047/BBJ/07/02',
  //     futuresClearingMembership: '15/AK-KBI/V/2003',
  //     approvalTradingParticipantSystem: '1156/BAPPEBTI/SI/3/2007',
  //     implementationAlternativeTradingSystem: 'PT. ROYAL ASSETINDO',
  //     website: 'www.sg-berjangka.com',
  //     ceo: 'Iriawan Widadi',
  //     director: 'George Setyawan Suryana Widjaya Ahmad Fauzi Dikki Soetopo',
  //     complianceDirector: 'M. Dwi Budhi Susmianto',
  //     eCaptchaPrivateKey: 'eCaptchaPrivateKey',
  //     reCaptchaPublicKey: 'reCaptchaPublicKey',
  //     // timeStampCreated: '2020-01-21T07:48:16.000Z',
  //     backupDate: null,
  //     phoenixCompanyID: 3,
  //   }),
  // );
  // console.log(await a.getCompanyData());
  // console.log(
  //   await a.insertCompanyData({
  //     companyID: '67aafbe6-d082-11e3-953d-00505681fuji',
  //     company_unit_id: 'bf2414f8-a84a-11e3-a4d9-005056810073',
  //     company_phone: '021-29675088',
  //     company_fax: '021-29675089',
  //     company_address: 'TCC Batavia Tower One Lt. 10\r\nJl. KH. Mansyur Kav. 126 Jakarta Pusat',
  //     company_email: 'berjangka@solidgold.co.id',
  //     senderEmail: 'noreply@solidgoldberjangka.co.id',
  //     companyRegistrationEmail: 'registrasi@solidgold.co.id',
  //     company_court: 'Pengadilan Negeri Jakarta Pusat',
  //     companyCode: 'SGB',
  //     companyName: 'PT. Andry BERJANGKA',
  //     companyLink: 'https://regol.solidgold.co.id/signup',
  //     etradeDemoLink: '',
  //     etradeRealLink: '',
  //     senderName: 'Registrasi Online PT SGB',
  //     smtpHost: 'mail.solidgoldberjangka.co.id',
  //     smtpPort: 25,
  //     smtpUser: 'noreply@solidgoldberjangka.co.id',
  //     smtpPass: 'noreplysgb123',
  //     bappebtiLicense: '161/BAPPEBTI/SI/IX/2002',
  //     futuresExchangeMembership: 'SPAB-047/BBJ/07/02',
  //     futuresClearingMembership: '15/AK-KBI/V/2003',
  //     approvalTradingParticipantSystem: '1156/BAPPEBTI/SI/3/2007',
  //     implementationAlternativeTradingSystem: 'PT. ROYAL ASSETINDO',
  //     website: 'www.sg-berjangka.com',
  //     ceo: 'Iriawan Widadi',
  //     director: 'George Setyawan Suryana Widjaya Ahmad Fauzi Dikki Soetopo',
  //     complianceDirector: 'M. Dwi Budhi Susmianto',
  //     eCaptchaPrivateKey: 'eCaptchaPrivateKey',
  //     reCaptchaPublicKey: 'reCaptchaPublicKey',
  //     // timeStampCreated: '2020-01-21T07:48:16.000Z',
  //     backupDate: null,
  //     phoenixCompanyID: 3,
  //   }),
  // );
})();
