import { Router } from 'express';
import { CompanyController } from './controller.company';

const router = Router();
const controller = new CompanyController();

/**
 * @swagger
 * /administration/company/insert-item:
 *    post:
 *     tags:
 *     - "company"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
 *         description: A list of company.
 */
router.post('/update', controller.postCompany);
/**
 * @swagger
 * /administration/company/get-item:
 *    get:
 *     tags:
 *     - "company"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
 *         description: A list of company.
 */
// router.get('/get-item', controller.getCompany);
/**
 * @swagger
 * /administration/company/get-item:
 *    get:
 *     tags:
 *     - "company"
 *     summary: Retrieve a list of companys.
 *     description: Retrieve a list of companys.
 *     responses:
 *       200:
 *         description: A list of company.
 */
router.get('/get-item', controller.getCompany);

export default router;
