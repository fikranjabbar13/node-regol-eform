import { NextFunction, Request, Response } from 'express';
import { CompanyData, CompanyService } from './services.company';
import { CompanyValidator } from './validator.company';
const company = new CompanyService();
const validator = new CompanyValidator();
export class CompanyController {
  async postCompany(req: Request, res: Response, next: NextFunction) {
    try {
      const validateResult: CompanyData = validator.insertValidate(req.body);
      const result = await company.insertCompanyData(validateResult);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }

  async getCompany(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await company.getCompanyData(req.query.id as string);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }
  // async getCompany(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     const result = await company.getCompanyData();
  //     res.json(result);
  //   } catch (error) {
  //     next(error);
  //   }
  // }
  // async updateCompany(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     console.log(validator.updateValidate);
  //     const validateResult: CompanyData = validator.updateValidate(req.body);
  //     const result = await company.updateCompanyData(validateResult.companyID as string, validateResult);

  //     res.json(result);
  //   } catch (error) {
  //     next(error);
  //   }
  // }
}
