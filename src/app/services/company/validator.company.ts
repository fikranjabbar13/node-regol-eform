import Joi from 'joi';
import HttpExpection from '../../errors/HttpExpection';
import { CompanyData } from './services.company';
export class CompanyValidator {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  insertValidate(data: CompanyData) {
    const schema = Joi.object({
      companyID: Joi.string().alphanum().min(3).max(30).required(),
      company_unit_id: Joi.string().alphanum().min(3).max(30).required(),
      company_phone: Joi.string().alphanum().min(3).max(30).required(),
      company_fax: Joi.string().alphanum().min(3).max(30).required(),
      company_address: Joi.string().alphanum().min(3).max(30).required(),
      company_email: Joi.string().alphanum().min(3).max(30).required(),
      senderEmail: Joi.string().alphanum().min(3).max(30).required(),
      companyRegistrationEmail: Joi.string().alphanum().min(3).max(30).required(),
      company_court: Joi.string().alphanum().min(3).max(30).required(),
      companyCode: Joi.string().alphanum().min(3).max(30).required(),
      companyName: Joi.string().alphanum().min(3).max(30).required(),
      companyLink: Joi.string().alphanum().min(3).max(30).required(),
      etradeDemoLink: Joi.string().alphanum().min(3).max(30).required(),
      etradeRealLink: Joi.string().alphanum().min(3).max(30).required(),
      senderName: Joi.string().alphanum().min(3).max(30).required(),
      smtpHost: Joi.string().alphanum().min(3).max(30).required(),
      smtpPort: Joi.number().required(),
      smtpUser: Joi.string().alphanum().min(3).max(30).required(),
      smtpPass: Joi.string().alphanum().min(3).max(30).required(),
      bappebtiLicense: Joi.string().alphanum().min(3).max(30).required(),
      futuresExchangeMembership: Joi.string().alphanum().min(3).max(30).required(),
      futuresClearingMembership: Joi.string().alphanum().min(3).max(30).required(),
      approvalTradingParticipantSystem: Joi.string().alphanum().min(3).max(30).required(),
      implementationAlternativeTradingSystem: Joi.string().alphanum().min(3).max(30).required(),
      website: Joi.string().alphanum().min(3).max(30).required(),
      ceo: Joi.string().alphanum().min(3).max(30).required(),
      director: Joi.string().alphanum().min(3).max(30).required(),
      complianceDirector: Joi.string().alphanum().min(3).max(30).required(),
      eCaptchaPrivateKey: Joi.string().alphanum().min(3).max(30).required(),
      reCaptchaPublicKey: Joi.string().alphanum().min(3).max(30).required(),
      timeStampCreated: Joi.string().alphanum().min(3).max(30).required(),
      backupDate: Joi.string().alphanum().min(3).max(30).required(),
      phoenixCompanyID: Joi.number().required(),
    });

    const result = schema.validate(data);

    if (result.error) {
      throw new HttpExpection(400, { message: 'Fail validate', data: result });
    } else return result.value;
  }
}
